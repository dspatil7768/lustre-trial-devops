#!/bin/bash

dnf install 'dnf-command(config-manager)' -y
echo "Configuring PowerTools repository"
yum config-manager --set-enabled powertools

echo "Downloading and installing kernel packges..."
cd /root && wget -r -l1 --no-parent -A kernel*4.18.0-240.1.1.*.rpm  http://mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages/
cd /root/mirror.centos.org/centos/8/BaseOS/x86_64/os/Packages && yum install kernel-*.rpm -y

echo "Installing required dependencies..."
yum install audit-libs-devel binutils-devel elfutils-devel kabi-dw ncurses-devel newt-devel numactl-devel openssl-devel pciutils-devel perl perl-devel python2 python3-docutils xmlto xz-devel elfutils-libelf-devel libcap-devel libcap-ng-devel llvm-toolset libyaml libyaml-devel kernel-rpm-macros libtool which rpm-build -y

echo  "Installing ZFS packages..."
yum install https://zfsonlinux.org/epel/zfs-release.el8_3.noarch.rpm -y
gpg --import --import-options show-only /etc/pki/rpm-gpg/RPM-GPG-KEY-zfsonlinux
dnf config-manager --disable zfs
dnf config-manager --enable zfs-kmod
yum install python3-pyzfs kmod-zfs* zfs* libzfs2-devel -y

echo  "Installing e2fsprogs..."
yum install e2fsprogs e2fsprogs-libs -y
