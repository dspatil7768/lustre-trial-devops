#!/bin/bash

# Packer downloaded and configured in the path
wget https://releases.hashicorp.com/packer/1.7.0/packer_1.7.0_linux_amd64.zip
unzip packer_1.7.0_linux_amd64.zip ; mv packer /usr/local/bin/ ; chmod +x /usr/local/bin/packer
packer --version

# Terraform downloaded and configured in the path
wget https://releases.hashicorp.com/terraform/0.14.7/terraform_0.14.7_linux_amd64.zip
unzip terraform_0.14.7_linux_amd64.zip ; chmod +x terraform ; cp terraform /usr/local/bin 
chmod +x /usr/local/bin/terraform
terraform --version 
